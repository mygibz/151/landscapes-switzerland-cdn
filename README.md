# Landscapes Switzerland - CDN

A CDN to host the images for [Landscapes Switzerland](https://gitlab.com/mygibz/151/landscapes-switzerland) (visit [landscapes-swiss.ch](https://landscapes-swiss.ch) for the full page)

## Functionality

This CDN is a docker container, which stores images and thumbnails (which it generates), for the [main application](https://gitlab.com/mygibz/151/landscapes-switzerland). This way the main app can be kept small and clean, without stressing the database or the main server too much.

All images need to be places into the *i* (short for images) folder, and then committed. The Gitlab-CI script will then go and generate thumbnails for them (this means, that the user does not have to load all the full size images images).